import Home from '@pages/Home';
import Chat from '@pages/Chat';
import ChatClient from '@pages/ChatComplete/ChatClient';
import Maps from '@pages/Maps';

export { Home, Chat, ChatClient, Maps }