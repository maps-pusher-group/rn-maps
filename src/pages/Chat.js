


import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { GiftedChat } from 'react-native-gifted-chat'
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';


class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = { messages: [] };
    this.onSend = this.onSend.bind(this);
  }


  componentWillMount() {
    
    //console.log(this.props.route.params.datos);
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: this.props.route.params.datos,
            avatar: 'https://facebook.github.io/react/img/logo_og.png',
          },
        },
      ],
    });
  }

  onSend(messages = []) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });
  }
  render() {
    return (
      <View style={{flex:1}}>
        <Header
          leftComponent={() => {
            return (
              <Icon
                name="chevron-left"
                size={16}
                color="#fff"
                onPress={() => {
                  this.props.navigation.pop()
                }}
              />

            )
          }}
          centerComponent={{ text: 'CHAT', style: { color: '#fff', fontWeight: "600" } }}
        />
       
        <GiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          user={{
            _id: 1,
          }}
        />
      </View>

    );
  }
}

export default Chat;