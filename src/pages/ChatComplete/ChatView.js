import React from 'react';
import { StyleSheet, Text, View, TextInput, FlatList, KeyboardAvoidingView } from 'react-native';
import { Container, Content, Footer, Button } from 'native-base';



export default class ChatView extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }

        this.handleSendMessage = this.onSendMessage.bind(this);
    }

    onSendMessage(e) { // (1)
        this.props.onSendMessage(this.state.text);
        this.refs.input.clear();
    }

    render() { // (2)
        return (
            <KeyboardAvoidingView
                style={styles.container}
                behavior="padding">


                <FlatList
                    data={this.props.messages}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => item.name + index}
                    styles={styles.messages} />


                <Footer>
                    <TextInput
                        autoFocus
                        keyboardType="default"
                        returnKeyType="done"
                        placeholder=" Enter Your message!"
                        placeholderTextColor={"rgba(0,0,0,0.5)"}
                        enablesReturnKeyAutomatically
                        style={styles.input}
                        blurOnSubmit={false}
                        onChangeText={(text) => this.setState({ text })}
                        onSubmitEditing={this.handleSendMessage}
                        ref="input"
                    />
                    <Button
                        style={{ flex: 0.2, height: '100%' }}
                        onPress={() => this.handleSendMessage()}>
                        <Text style={styles.button} > Send </Text>
                    </Button>
                </Footer>

            </KeyboardAvoidingView>
        );
    }

    renderItem({ item }) { // (3)
        const action = item.action;
        const name = item.name;

        if (action == 'join') {
            return <Text style={styles.joinPart}>{name} has joined</Text>;
        } else if (action == 'part') {
            return <Text style={styles.joinPart}>{name} has left</Text>;
        } else if (action == 'message') {
            return <Text>{name}: {item.message}</Text>;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        //alignItems: 'flex-start',
        //justifyContent: 'flex-start',
        //paddingTop: 50
    },
    messages: {
        //alignSelf: 'stretch'
    },
    input: {
        flex: 1,
        //alignSelf: 'stretch',
        backgroundColor: 'lightgrey',
    },
    button: {
        flex: 1,
        textAlign: 'center',
        color: 'white',
        fontWeight: '600'
    },
    joinPart: {
        fontStyle: 'italic'
    }
});